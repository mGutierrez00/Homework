import java.util.Random;

public class HashProbing {

    public static void main(String[] args) {
       //int N = Integer.parseInt(args[0]);
       int N=100000000;
       int[] countArray = new int[1000];
       int[] intArray = new int[N];
       Random rand = new Random();
       for(int i = 0; i <= N/2; i++) {
           int temp = rand.nextInt(N);
           int counter = 0;
           while (!(intArray[temp] == 0)) {
               temp++;
               if(temp > N-1) {
                   temp = 0; 
               }
           }
           intArray[temp] = rand.nextInt(N);
       }
       double sum = 0; 
       
       for(int i =0; i < 1000; i++) {
           int temp = rand.nextInt(N);
           int count = 0;
          
           while (!(intArray[temp] == 0)) {
               
               temp++;
               count++;
               if(temp > N-1) {
                   temp = 0; 
               }
               
           }
           sum = sum + count;
           countArray[i]=count;
         
       }
       
       System.out.println(sum/1000);

    }
    

}
