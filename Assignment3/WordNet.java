import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.LinearProbingHashST;
import edu.princeton.cs.algs4.RedBlackBST;
import edu.princeton.cs.algs4.Bag;
import edu.princeton.cs.algs4.BreadthFirstDirectedPaths;
import edu.princeton.cs.algs4.Digraph;

public class WordNet {

    HashMap<String, Bag<Integer>> h = new HashMap<String, Bag<Integer>>();
    //HashMap<String, Bag<String>> h1 = new HashMap<String, Bag<String>>();
   // HashMap<Integer, String> h = new HashMap();
    HashMap<Integer, String> h1 = new HashMap();

    Digraph myDi;

    // constructor takes the name of the two input files
    public WordNet(String synsets, String hypernyms) throws IOException {

        String line = null;
        int size = 0;
        FileReader synReader = new FileReader(synsets);
        BufferedReader synbufferedReader = new BufferedReader(synReader);
        while ((line = synbufferedReader.readLine()) != null) {

            
            
            
            String[] lineSplit = line.split(",");

            String[] splitSplit = lineSplit[1].split(" ");
            
            //h.put(Integer.parseInt(splitSplit[0]), lineSplit[0]);
            
            Bag<String> bagh1 = new Bag<String>();
            for (String n : splitSplit) {
                bagh1.add(n);
                if (h.containsKey(n)) {
                    h.get(n).add(Integer.parseInt(lineSplit[0]));
                } else {
                    Bag<Integer> bagh = new Bag<Integer>();
                    bagh.add(Integer.parseInt(lineSplit[0]));
                    h.put(n, bagh);
                }
            }
            //h1.put(lineSplit[0], bagh1);
             
             
            h1.put(Integer.parseInt(lineSplit[0]), splitSplit[0]);

            size++;

        }

        myDi = new Digraph(size);

        FileReader hypReader = new FileReader(hypernyms);
        BufferedReader hypbufferedReader = new BufferedReader(hypReader);

        while ((line = hypbufferedReader.readLine()) != null) {

            String[] lineSplit = line.split(",");

            for (int i = 1; i < lineSplit.length; i++) {
                myDi.addEdge(Integer.parseInt(lineSplit[0]), Integer.parseInt(lineSplit[1]));
            }

        }

    }

    // all WordNet nouns
    public Iterable<String> nouns() throws java.lang.NullPointerException {
        return null;

    }

    // is the word a WordNet noun?
    public boolean isNoun(String word) throws java.lang.NullPointerException {
        return h.containsKey(word);
    }

    // a synset (second field of synsets.txt) that is a shortest common ancestor
    // of noun1 and noun2 (defined below)
    public String sca(String noun1, String noun2) throws java.lang.IllegalArgumentException {
        ShortestCommonAncestor sca = new ShortestCommonAncestor(myDi);
        System.out.println(sca.ancestor(Integer.parseInt(noun1), Integer.parseInt(noun2)));
        return "Test";
    }

    // distance between noun1 and noun2 (defined below)
    public int distance(String noun1, String noun2) throws java.lang.IllegalArgumentException {

        // BreadthFirstDirectedPaths bfspaths = new BreadthFirstDirectedPaths(myDi,
        // Integer.parseInt(noun1));

        ShortestCommonAncestor sca = new ShortestCommonAncestor(myDi);
        int length = sca.length(Integer.parseInt(noun1), Integer.parseInt(noun2));
        return (length);

    }

    public static void main(String[] args) throws IOException {

        WordNet wordnet = new WordNet("synsets.txt", "hypernyms.txt");

        // System.out.println(wordnet.h1.get("73614").toString());
        for (Integer n : wordnet.h.get("cat"))
            System.out.println(wordnet.h1.get(n));
        ;
        // int distance = wordnet.distance(wordnet.h.get("75058").toString(),
        // wordnet.h.get("68015").toString());
        // System.out.println(length);
        // System.out.println(distance);

    }

}