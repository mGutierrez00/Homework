//Mathew Gutierrez
//13 Sep 2017


import java.util.Random;

public class RandomDemo {
    public static void main(String[] args) {
        //Random random = new Random(123456L);
        Random random = new Random();
        boolean a = random.nextBoolean(); //true or false
        int b = random.nextInt(9);
        int c = random.nextInt(100);
        double d = random.nextDouble();
        double e = random.nextGaussian();
        StdOut.println(a);
        StdOut.println(b);
        StdOut.println(c);
        StdOut.println(d);
        StdOut.println(e);
    }
}
