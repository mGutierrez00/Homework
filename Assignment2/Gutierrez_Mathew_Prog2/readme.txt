/******************************************************************************
 *  Name:    Mathew Gutierrez 
 *
 *  Hours to complete assignment (optional):
 * 	15
 ******************************************************************************/

Programming Assignment 3: Autocomplete


/******************************************************************************
 *  Describe how your firstIndexOf() method in BinarySearchDeluxe.java
 *  finds the first index of a key that equals the search key.
 *****************************************************************************/

The firstIndexOf() Method uses a binary search method 
until it reaches a range of matches, it then iterates through the matches until it reaches 
the first, then returns that. 

/******************************************************************************
 *  What is the order of growth of the number of compares (in the
 *  worst case) that each of the operations in the Autocomplete
 *  data type make, as a function of the number of terms n and the
 *  number of matching terms m?
 *
 *  Recall that with order-of-growth notation, you should discard
 *  leading coefficients and lower-order terms, e.g., m^2 + m log n.
 *****************************************************************************/

constructor: N log(n) 

allMatches(): N + Mlog(M)

numberOfMatches():  log(n)


/******************************************************************************
 *  Known bugs / limitations.
 *****************************************************************************/

Does not like when no matches are found. Tends to lock up.

/******************************************************************************
 *  Describe whatever help (if any) that you received.
 *  Don't include readings or lectures, but do include
 *  any help from people (including course staff, lab TAs,
 *  classmates, and friends) and attribute them by name.
 *
 *  Also include any resources (including the web) that you may
 *  may have used in creating your design.
 *****************************************************************************/

I got help from both Travis and Jimmy on issues and small bugs I encountered.

/******************************************************************************
 *  Describe any serious problems you encountered.                    
 *****************************************************************************/

No serious problems. Some small difficulty Understanding expectations from just the API provided.


/******************************************************************************
 *  List any other comments here. Feel free to provide any feedback   
 *  on how much you learned from doing the assignment, and whether    
 *  you enjoyed doing it.                                             
 *****************************************************************************/
I enjoyed this assignment quite a lot. It really forced me to understand Binary searches
and solidified my knowledge. 
  
