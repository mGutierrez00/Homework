1) 
had issues with alg4 on my windows computer, so I'm hoping there are no issues with args passed in for size of priority queue.

runtimes
each initialization, halving, filling and emptying is done 10 times, and averaged. 

n = 100		1139533
n = 1000	1456352
n = 10000	2255945
n = 100000	5167173
n = 1000000	37797484
n = 10000000	5765490 (???) less than previous? why? 

2) 
could not run n = 10^9. intractable with my setup for some reason.
		Construction	Removal
n = 10^3	1127722		1664268
n = 10^6	45095282	917029040
n = 10^7	3135144106	6709624229

The instructions are unclear to me, and I'm not sure if I'm being asked to implement a min-heap on my own, or if i should be using princetons supplied.

I presume that doing so would result in longer times, while the improved sort will reduce the time. 

