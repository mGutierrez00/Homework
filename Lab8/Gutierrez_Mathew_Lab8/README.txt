2a) We can sort in linearithmic guaranteed time and constant extra space by using heapsort. As it is an in place worst time linearithmic approach.

2b) The assumption that is made about our hashing function is that of uniform hashing. 

Both methods work. The run times of the heap is linearithmic, and the hash *should* be Linear with respect to size of array.
Obviously, the larger the array, the longer the runtime. This is more apparent in heapsort, as it is a linearithmic approach. 
Hash sorting took 19 seconds for 10m and heap took 34 seconds for the same. 