/******************************************************************************
 *  Compilation:  javac-algs4 BinarySearchDeluxe.java
 *  Execution:    java-algs4 BinarySearchDeluxe 
 *  Dependencies: StdLib Algs4
 *                
 *  
 *  Binary Search Implementation
 * 
 ******************************************************************************/
import java.util.Arrays;
import java.util.Comparator;

public class BinarySearchDeluxe {

	public static <Key> int firstIndexOf(Key[] a, Key key, Comparator<Key> comparator) {
		if (a == null || key == null || comparator == null) {
			throw new NullPointerException();
		}
		
		int low = 0;
		int high = a.length - 1;
		
		if (comparator.compare(a[0], key) == 0) {
			return 0;
		}

		while (low <= high) {
			int mid = low + (high - low) / 2;
			if (comparator.compare(key, a[mid]) < 0)
				high = mid - 1;
			else if (comparator.compare(key, a[mid]) > 0)
				low = mid + 1;
			else if (comparator.compare(a[mid - 1], a[mid]) == 0)
				high = mid - 1;
			else
				return mid;
		}
		return -1;
	}

	
	public static <Key> int lastIndexOf(Key[] a, Key key, Comparator<Key> comparator) {

		int low = 0;
		int high = a.length - 1;

		while (low <= high) {
			int mid = low + (high - low) / 2;
			if (comparator.compare(key, a[mid]) < 0)
				high = mid - 1;
			else if (comparator.compare(key, a[mid]) > 0)
				low = mid + 1;
			else if (comparator.compare(a[mid +1], a[mid]) == 0)
				low = mid + 1; 
			else
				return mid;
		}
		return -1;
	}

	
    // Unit Testing
	public static void main(String[] args) {
		Term[] terms = new Term[5];
		terms[0] = new Term("Butt", 45);
		terms[1] = new Term("Ard", 40);
		terms[2] = new Term("Ceril", 35);
		terms[3] = new Term("Ceril", 55);
		terms[4] = new Term("Elk", 25);

		String prefix = args[0];
		Arrays.sort(terms);
		System.out.println(firstIndexOf(terms, new Term(prefix, 0), Term.byPrefixOrder(prefix.length())));
		System.out.println(lastIndexOf(terms, new Term(prefix, 0), Term.byPrefixOrder(prefix.length())));

	}

}
