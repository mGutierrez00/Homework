1) Run empirical studies to compute the average and standard deviation of the average length of a path to a random node in a BST built by insertion of N random keys into an initially empty tree, for N from 100 to 12,800 

outputs are as follows for n = 100 -> 12800 where t = 1000 each. 

n = 100
7.898785907492533
0.03227391992528153

n = 200
7.184139053634623
0.01892944218926982

n = 400
9.046398564021066
0.026170844102044497

n = 800
9.946819319553912
0.01594392957987153

n = 1600
11.658557067967916
0.020933454243619615

n = 3200
12.90711271391017
0.07008540387481926

n = 6400
14.154823282168307
0.03410543044757691

n = 12800
16.08774925961917
0.024424469330210936

Notes on experiment.
Times seem to vary largely, even given 1000 repetitions at each size. Only guess is that process would be longer/shorter depending on other activities on computer. 
Calculated stddev in program. 

2) Linear-Probing Distribution
outputs are as follows for n = 1000 - 1000000
1.208
1.499
1.571
1.627
 

It appears to somewhat support the proposition M in the book. As the lists grow larger, the tendency is for the miss time to increase tot he proposed value. 