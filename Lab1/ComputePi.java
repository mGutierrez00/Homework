//Mathew Gutierrez
//13 Sep 2017


import java.util.Random;

public class ComputePi {
    //initialize varaibles
    static Random random = new Random();
    
    
    //Runs N times to estimate value of Pi using random numbers and Euclidian distance prints to console.
    public static void main(String[] args) {
        int inside = 0;
        int N = Integer.parseInt(args[0]);
        for(int i = 1; i<N ; i++) {
            double x = random.nextDouble();
            double y = random.nextDouble();
            if(Math.sqrt(x*x+y*y)<=1) {
                inside++;
            }
            
        }
        double pi = 4*(double)inside/(double)N;
        
        System.out.println(pi);
    }

}
