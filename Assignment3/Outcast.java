import java.io.IOException;

import edu.princeton.cs.algs4.Bag;

public class Outcast {
    WordNet wordnet;
    Bag<String> bag = new Bag<String>();

    public Outcast(WordNet wordnet) {
        this.wordnet = wordnet;
        // constructor takes a WordNet object
    }

    public String outcast(String[] nouns) {
        // given an array of WordNet nouns, return an outcast
        Integer candidate = 0;
        for (String noun : nouns) {
            int minSum = 100000;
            
            for (Integer a : wordnet.h.get(noun)) {
               
                for(String c : nouns) {
                    int sum = 0;
                    for(Integer b : wordnet.h.get(c)) {
                        sum = sum + wordnet.distance(a.toString(), b.toString());
                        //System.out.println(wordnet.h1.get(a) + " : " + wordnet.h1.get(b) + " : " + wordnet.distance(a.toString(), b.toString()));
                    }
                    //System.out.println(sum);
                    if (sum < minSum) {
                        minSum = sum;
                        candidate = a;
                    }
                    
                    //System.out.println(a);
                }
            }
            
             bag.add(candidate.toString());

        }
        String outcast = "";
        int sum = 0;
        int maxSum = 0;
        for (String a : bag) {
            sum = 0;
            for (String b : bag) {
                sum = sum + wordnet.distance(a, b);
                //System.out.println(wordnet.h1.get(Integer.parseInt(a)) + " : " + wordnet.h1.get(Integer.parseInt(b)) + " : " + wordnet.distance(a, b));
            }
            //System.out.println(sum);
            if (sum > maxSum) {
                maxSum = sum;
               
                outcast = a;
               
            }
            //System.out.println(a);

        }
        
      
        return wordnet.h1.get(Integer.parseInt(outcast));

    }

    public static void main(String[] args) throws IOException {
        WordNet wordnet = new WordNet("synsets.txt", "hypernyms.txt");
        Outcast outcast = new Outcast(wordnet);

        In in = new In("outcast11.txt");
        String[] nouns = in.readAllStrings();

        StdOut.println("outcast5.txt" + ": " + outcast.outcast(nouns));

    }
}