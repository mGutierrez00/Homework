/******************************************************************************
 *  Compilation:  javac FixedSizedStack.java
 *  Execution:    java FixedSizedStack < input.txt
 *  Dependencies: StdIn.java StdOut.java
 
 *  % java FixedSizedStack < tobe.txt
 ******************************************************************************/

import java.util.Iterator;
import java.util.NoSuchElementException;

public class FixedSizedStack<Item> implements Iterable<Item> {
	private int capacity;
	private Deque deck = new Deque();

	public class Node {
		private Item item;
		private Node next;
		private Node previous;
	}

	public FixedSizedStack(int c) { // construct an empty FS-stack, with capacity c
		this.capacity = c;
	}

	public boolean isEmpty() {// is it empty?
		return deck.isEmpty();
	}

	public int size() {// return the number of items on the FS-stack
		return deck.size();
	}

	public void push(Item item) {// add the item to the front, and remove an item from the back if capacity is
									// reached
		deck.addFirst(item);
		if (deck.size() > capacity)
			deck.removeLast();
	}

	public Item pop() {// remove the item from the front
		return (Item) deck.removeFirst();
	}

	public Iterator<Item> iterator() {
		return new ListIterator();
	}

	private class ListIterator implements Iterator<Item> {
		private Deque.Node current = deck.getFirst();

		public void remove() throws UnsupportedOperationException {
			throw new UnsupportedOperationException();
		}

		public boolean hasNext() {
			return ((current != null));
			// how to check if the iterator still has more things to work with?
		}

		public Item next() {
			if (!hasNext()) {
				throw new NoSuchElementException();
			}
			Item item = (Item) current.item;
			current = current.next;

			return item;

			// how to walk along the list, starting from first ?
			// watch out for next() call when there is no next item
		}
	}

	public static void main(String[] args) { // unit testing (required)

		FixedSizedStack<String> queue = new FixedSizedStack<String>(10);

		while (!StdIn.isEmpty()) {

			String item = StdIn.readString();
			queue.push(item);

		}

		while (!queue.isEmpty()) {

			System.out.println(queue.pop());
		}

	}
}