/*******************************************************************************
 *  Compilation: javac Subset.java Execution: java Subset < input.txt
 *  Execution:    java Subset (integer) < input.txt
 *  Dependencies: StdIn.java StdOut.java
 * 
 *  Subset which takes in strings from StdIn and an argument for a size of subset and returns randomly from bag.
 ******************************************************************************/
public class Subset {

    public static void main(String[] args) {
        RandomizedBag bag = new RandomizedBag();

        while (!StdIn.isEmpty()) {

            String item = StdIn.readString();
            bag.put(item);
        }
        for (int i = 0; i < Integer.parseInt(args[0]); i++) {
            System.out.println(bag.get());
        }
    }

}
