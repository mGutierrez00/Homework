/******************************************************************************
 *  Compilation:  javac-algs4 TestAlgs4.java
 *  Execution:    java-algs4 TestAlgs4 
 *  Dependencies: StdDraw.java
 *                http://algs4.cs.princeton.edu/mac/cover.jpg
 *  
 *  Percolates and creates statistical values from percolation
 * 
 ******************************************************************************/

import java.util.Random;
import edu.princeton.cs.algs4.StdStats;
import edu.princeton.cs.algs4.Stopwatch;

public class PercolationStats {

    // Initialize variables
    private final double[] stats;
    private final Random random = new Random();
    private final double stdDev;
    private final double mean;
    private final int runs;

    
    // Constructor to instantiate percolate with size gridSize with runs number of runs.
    public PercolationStats(int gridSize, int runs)  {
        if (gridSize <= 0 || runs <= 0) {
            throw new java.lang.IllegalArgumentException();
        }
        
        this.runs = runs;
        stats = new double[runs];
        for (int i = 0; i < runs; i++) {

            Percolation temp = new Percolation(gridSize);

            while (!(temp.percolates())) {

                
                int a = random.nextInt(gridSize);
                int b = random.nextInt(gridSize);
                if (!(temp.isOpen(a, b))) {
                    temp.open(a, b);

                } 
            }

            stats[i] = ((double) temp.numberOfOpenSites() / (gridSize * gridSize));

        }
        mean = mean(stats);
        stdDev = stddev(stats);

        // Print out statistics
        
        System.out.println(mean);
        System.out.println(stdDev);
        System.out.println(confidenceLow());
        System.out.println(confidenceHigh());

    }

    
    // Statistics methods
    public double mean(double[] array) {
        return StdStats.mean(array);
    }

    public double stddev(double[] array) {
        return StdStats.stddev(array);
    }

    public double confidenceLow() {
        return (mean - (1.96 * (Math.sqrt(stdDev))) / Math.sqrt(runs));
    }

    public double confidenceHigh() {
        return (mean + (1.96 * (Math.sqrt(stdDev))) / Math.sqrt(runs));
    }
    
    // Unit testing in main.
    public static void main(String[] args) {
        Stopwatch clock = new Stopwatch();
        PercolationStats temp = new PercolationStats(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        System.out.println(clock.elapsedTime());
        
    }


}
