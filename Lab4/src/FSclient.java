/******************************************************************************
 * Compilation: javac FSclient.java Execution: java FSclient < input.txt
 * Dependencies: StdIn.java StdOut.java
 * 
 * % java-alg4 FSclient (integer) < tobe.txt
 ******************************************************************************/
public class FSclient {

	public static void main(String[] args) { // unit testing (required)

		FixedSizedStack<String> stack = new FixedSizedStack<String>(Integer.parseInt(args[0]));

		while (!StdIn.isEmpty()) {
			String item = StdIn.readString();
			if (!item.equals("-"))
				stack.push(item);
			else if (!stack.isEmpty())
				StdOut.print(stack.pop() + " ");
		}
		StdOut.println("(" + stack.size() + " left on stack)");
	}

}
