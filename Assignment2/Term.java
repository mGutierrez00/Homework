/******************************************************************************
 *  Compilation:  javac-algs4 Term.java
 *  Execution:    java-algs4 Term 
 *  Dependencies: StdLib Algs4
 *  
 *  Term object that implements comparable
 * 
 ******************************************************************************/
import java.util.Arrays;
import java.util.Comparator;

public class Term implements Comparable<Term> {
    private final String query;
    private final long weight;

    // Initializes a term with the given query string and weight.
    public Term(String query, long weight) throws NullPointerException {

        if (weight < 0) {
            throw new IllegalArgumentException();
        }

        this.query = query;
        this.weight = weight;
    }

    // Compares the two terms in descending order by weight.
    public static Comparator<Term> byReverseWeightOrder() {
        return new Comparator<Term>() {
            public int compare(Term v, Term w) {
                if (v.weight > w.weight) {
                    return -1;
                } else if (v.weight < w.weight) {
                    return 1;
                } else {
                    return 0;
                }
            }
        };
    }

    // Compares the two terms in lexicographic order but using only the first r
    // characters of each query.
    public static Comparator<Term> byPrefixOrder(int r) {

        if (r < 0) {
            throw new IllegalArgumentException();
        }

        final int rr = r;
        return new Comparator<Term>() {
            public int compare(Term v, Term w) {
                return v.query.substring(0, rr).compareTo(w.query.substring(0, rr));
                // return -1, 1, or 0, depending on how terms relate
                // Be careful to only consider the first rr letters.
            }
        };
    }

    // Compares the two terms in lexicographic order by query.
    public int compareTo(Term that) {
        return this.query.compareTo(that.query);
    }

    // Returns a string representation of this term in the following format:
    // the weight, followed by a tab, followed by the query.
    public String toString() {

        return weight + "\t" + query;

    }

    // Unit Testing
    public static void main(String[] args) {
        Term[] terms = new Term[5];
        terms[0] = new Term("Butt", 45);
        terms[1] = new Term("Ard", 40);
        terms[2] = new Term("Ceril", 35);
        terms[3] = new Term("Test", 30);
        terms[4] = new Term("Elk", 25);

        Arrays.sort(terms);
        for (Term t : terms) {
            StdOut.println(t);
        }

        StdOut.println("");

        Arrays.sort(terms, Term.byReverseWeightOrder());
        for (Term t : terms) {
            StdOut.println(t);
        }

        StdOut.println("");

        Arrays.sort(terms, Term.byPrefixOrder(1));

        for (Term t : terms) {
            StdOut.println(t);
        }

    }

}
