/******************************************************************************
 *  Compilation:    javac Deque.java 
 *  Execution:      java Deque < input.txt 
 *  Dependencies:   StdIn.java StdOut.java
 * 
 *  generalization of a stack and queue that supports adding and removing items
 *  form either the front or the back of the data structure.
 *
 ******************************************************************************/

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item> {

    private int n; // number of elements on queue
    private Node first; // beginning of queue
    private Node last; // end of queue

    // helper linked list class
    private class Node {
        private Item item;
        private Node next;
        private Node previous;
    }

    public Deque() { // construct an empty deque
        first = null;
        last = null;
        n = 0;
        // what needs to be initialized here?
    }

    public boolean isEmpty() { // is the deque empty?
        return n == 0;
    }

    public int size() { // return the number of items on the deque
        return n;
    }

    // Add to front of stack
    public void addFirst(Item item) throws NullPointerException { // add the item to the front

        if (item == null) {
            throw new NullPointerException("Item cannot be NULL");
        }

        if (isEmpty()) {
            first = new Node();
            first.item = item;
            last = first;

        } 
        else {

            Node oldfirst = first;
            first = new Node();
            first.previous = oldfirst;
            oldfirst.next = first;
            first.item = item;

        }
        n++;
    }

    // Add to end of stack
    public void addLast(Item item) throws NullPointerException { // add the item to the end

        if (item == null) {
            throw new NullPointerException("Item cannot be NULL");
        }

        if (isEmpty()) {
            last = new Node();
            last.item = item;
            first = last;

        } 
        else {

            Node oldlast = last;
            last = new Node();
            last.next = oldlast;
            oldlast.previous = last;
            last.item = item;

        }
        n++;
    }

    // Remove from front of stack
    public Item removeFirst() throws NoSuchElementException { // remove and return the item from the front

        if (isEmpty()) {
            throw new NoSuchElementException("Queue underflow");
        }

        else {

            Item item = first.item;
            first = first.previous;
            n--;
            return item;
        }

    }

    // Remove from end of stack
    public Item removeLast() { // remove and return the item from the end
        if (isEmpty()) {
            throw new NoSuchElementException("Queue underflow");
        }

        else {
            Item item = last.item;
            last = last.next;
            n--;
            return item;
        }
    }

    public Iterator<Item> iterator() {
        return new ListIterator();
    }

    private class ListIterator implements Iterator<Item> {
        private Node current = first;

        public void remove() throws UnsupportedOperationException {
            throw new UnsupportedOperationException();
        }

        public boolean hasNext() {
            return ((current != null));
            // how to check if the iterator still has more things to work with?
        }

        public Item next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            Item item = current.item;
            current = current.next;
            return item;

            // how to walk along the list, starting from first ?
            // watch out for next() call when there is no next item
        }
    }

    // Unit testing
    public static void main(String[] args) {
        Deque<String> queue = new Deque<String>();

        while (!StdIn.isEmpty()) {
            String item = StdIn.readString();
            queue.addFirst(item);
            queue.addLast(item);
        }

        while (!queue.isEmpty()) {
            StdOut.print(" " + queue.removeFirst());
            StdOut.print(" " + queue.removeLast());
        }

    }
}
