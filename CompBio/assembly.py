from __future__ import print_function
import sys
import numpy
import time
from operator import itemgetter

class Node(object):
    def __init__(self, previous, next, depth, walked, base):
        self.previous = previous
        self.next = next = []
        self.depth = depth
        self.walked = walked = []
        self.base = base


class seqTree(object):
    def __init__(self,reads):
        self.reads = reads
        self.head = None
        self.maxDepth = 0
        self.bestMatch = []
        self.size = 0
        self.matchList = []
        for idx, read in enumerate(reads):
            suffixes = [read[i:] for i in range(1,len(read))]
            for suffix in suffixes:
                current = self.head
                depth = 0
                for base in suffix:
                    if self.head == None:
                        node = Node(None, [],depth,idx,'$')
                        self.size+=1
                        self.head = node
                        current = self.head
                    else:
                        exists = False
                        for edge in current.next:
                            if base == edge.base:
                                current = edge
                                if idx not in edge.walked:
                                    edge.walked.append(idx)
                                if len(edge.walked)>1 and depth > 2:
                                    self.matchList.append((depth,edge.walked))
                                exists = True
                        if not exists:
                            node = Node(current,[],depth,[idx],base)
                            self.size+=1
                            current.next.append(node)
                            current = node
                    depth += 1
        #print(self.bestMatch,self.maxDepth)
    def removeIdx(self, root, aligned):
        for edge in root.next:
            for idx in aligned:
                if idx in edge.walked:
                    if len(edge.walked)== 1:
                        root.next.remove(edge)
                    self.removeIdx(edge, aligned)
                    self.size-=1
        for idx in aligned:
            if idx in root.walked:
                root.walked.remove(idx)
                self.size-=1

    def addAligned(self, root, aligned,idx):
        suffixes = [aligned[i:] for i in range(1,len(aligned))]
        for suffix in suffixes:
            current = self.head
            depth = 0
            for base in suffix:
                exists = False
                for edge in current.next:
                    if base == edge.base:
                        current = edge

                        if idx not in edge.walked:
                            edge.walked.append(idx)
                            if len(edge.walked)>1 and depth > 2:
                                self.matchList.append((depth,edge.walked))
                        exists = True
                if not exists:
                    node = Node(current,[],depth,[idx],base)
                    print(base,end = "")
                    self.size+=1
                    current.next.append(node)
                    current = node
            depth += 1

    def printTree(self, root):
        while len(root.next) > 0:
            for base in root.next:
                self.printTree(base)
            print(root.base, end = '')
            return (root.base)
        print("")

def combine(str1, str2):
    def find(str1,str2):
        bestAlign = 0
        bestScore = 0
        for idx,char in enumerate(str1):
            i = 0
            score = 0
            if (char == str2[i]):
                j = i
                while(idx+i < len(str1) and j < len(str2) and str1[idx+i] == str2[j]):
                    score+=1
                    i+=1
                    j+=1
                if (idx+i == len(str1) or j == len(str2)) and (bestScore < score):
                    bestScore = score
                    bestAlign = idx
                i+=1
                score = 0
        return[bestAlign,bestScore]
    def align(str1, str2,oneAndTwo, TwoAndOne):
        if(oneAndTwo[1]+TwoAndOne[1] == 0):
            return "Error: No overlap found"
        elif(oneAndTwo[1]>TwoAndOne[1]):
            return str1+(str2[oneAndTwo[1]:])
        else:
            return str2+(str1[TwoAndOne[1]:])

    oneAndTwo = find(str1,str2)
    TwoAndOne = find(str2,str1)
    return(align(str1,str2,oneAndTwo, TwoAndOne))

def build(self):
    for idx,read in enumerate(reads):
        suffixes = [read[i:] for i in range(1,len(read))]

def traverse(root, head):
    if leaf:
        if match is not None:
            return
    if root.a is not None:
        traverse(root, root.a)
    if root.g is not None:
        traverse(root, root.g)
    if root.t is not None:
        traverse(root, root.t)
    if root.c is not None:
        traverse(root, root.c)



    root.match = head



def main(argv):
    reads = []
    smallReads = argv[0]
    with open(smallReads) as file:
        for line in file:
                line = line.strip()
                reads.append(line)

    #print reads

    test = seqTree(reads)

    test.matchList = sorted(test.matchList,key=itemgetter(0),reverse = True)
    while(len(test.matchList) > 1):
        i = 0
        for idx,match in enumerate(test.matchList):
            #print(match)
            if(len(match[1])>1):
                i = idx
                str1 = reads[match[1][0]]
                str2 = reads[match[1][1]]
                test.addAligned(test.head,combine(str1,str2),match[1][0])
                print(combine(str1,str2))
                test.removeIdx(test.head, match[1][1:])

                print(test.matchList)
                break
        del test.matchList[i]
        test.matchList = sorted(test.matchList,key=itemgetter(0),reverse = True)
    test.printTree(test.head)





if __name__=='__main__':
  main(sys.argv[1:])
