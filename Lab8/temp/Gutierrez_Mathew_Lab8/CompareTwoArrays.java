
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.LinearProbingHashST;
import edu.princeton.cs.algs4.MaxPQ;


public class CompareTwoArrays
{
  

    private static boolean compareWithHeap (In inA, In inB, int size) {
        
        boolean match = true;
        
        MaxPQ heap1 = new MaxPQ(size);    
        MaxPQ heap2 = new MaxPQ(size);  
        
        while (!inA.isEmpty()) {
            int item = inA.readInt();
            heap1.insert(item);
        }
        while(!inB.isEmpty()) {
            int item = inB.readInt();
            heap2.insert(item);
           
        }
        
       while(!heap1.isEmpty()) {
           
           if(!(heap1.delMax().equals(heap2.delMax()))) {
               match = false;          
           }else {
               
           }
       }
        return match;
    }
    
    private static boolean compareWithHash (In inA, In inB, int size) {
        
        boolean match = true;
        
        LinearProbingHashST hash = new LinearProbingHashST(size);
        
        while (!inA.isEmpty()) {
            int item = inA.readInt();

            hash.put(item , " ");
            
        }
        while (!inB.isEmpty()) {
            int item = inB.readInt();
            if(hash.contains(item)) {
                hash.delete(item);
            }else {
                match = false;
            }
        }
        if(hash.size() > 0) {
            System.out.println("not zero");
        }
        
        return match;
    }
    
    public static void main(String[] args) {
       
        //if (args.length < 3) {
        //    StdOut.println("Usage: java-algs4 CompareTwoArrays filenameA filenameB [heap/hash]");
        //    System.exit(1);
        //}
        String filenameA = "single_arrays/10mA.txt";//args[0];
        String filenameB = "single_arrays/10mB.txt";//args[0];
        String method    = "heap";//args[0];
        
        if ( !method.equals("heap") && !method.equals("hash") ) {
            StdOut.println("Usage: java-algs4 CompareTwoArrays filenameA filenameB [heap/hash]");
            System.exit(1);
        }
        
        In inA = new In(filenameA);
        int nA = inA.readInt();

        In inB = new In(filenameB);
        int nB = inB.readInt();

        if (nA != nB) {
            StdOut.println("Arrays are not the same size, so not equivalent");
            System.exit(0);
        }
        
        boolean match = false;
        
        StopwatchCPU sw = new StopwatchCPU();
        
        if (method.equals("heap")) {
            match = compareWithHeap(inA, inB, nA);
        } else {
            match = compareWithHash(inA, inB, nA);
        }
        
        
        double elapsed = sw.elapsedTime();
        

        StdOut.println("The two input arrays do " + (match?"":"not ") + "match" );
        StdOut.println("elapsed time: " + elapsed + " seconds");
        
    }

   
}

