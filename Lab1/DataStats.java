//Mathew Gutierrez
//13 Sep 2017


public class DataStats{
    //initialize variables
    double[] dubArray;
    double mean;
    int index = 0;
    
    //constructor with size n
    public DataStats(int N) {
        dubArray = new double[N];
    }
    
    //compute mean
    public double mean() {
        double sum = 0;
        for(int i = 0; i < dubArray.length; i++) {
            sum += dubArray[i];
        }
        return mean = sum/(index);
    }
    
    //append to array
    public void append(double in){
        try {
        dubArray[index]= in;
        index++;
        } catch(Exception e){
            System.out.println("Index out of bounds");
        }
    }
}
