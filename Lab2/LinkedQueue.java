/******************************************************************************
 *  Compilation:  javac LinkedQueue.java
 *  Execution:    java LinkedQueue < input.txt
 *  Dependencies: StdIn.java StdOut.java
 *  Data files:   http://algs4.cs.princeton.edu/13stacks/tobe.txt  
 *
 *  A generic queue, implemented using a singly-linked list.
 *
 *  % java Queue < tobe.txt 
 *  to be or not to be (2 left on queue)
 *
 ******************************************************************************/

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedQueue<Item> implements Iterable<Item> {
    private int n; // number of elements on queue
    private Node first; // beginning of queue
    private Node last; // end of queue

    // helper linked list class
    private class Node {
        private Item item;
        private Node next;
    }
    
    public LinkedQueue() {
        first = null;
        last  = null;
        n = 0;
        // what needs to be initialized here?
    }

    public boolean isEmpty() {
        return first == null;
    }

    public int size() {
        return n;
    }


    public void enqueue(Item item) {
        Node oldlast = last;
        last = new Node();
        last.item = item;
        last.next = null;
       
        if (isEmpty()) {
            first = last;
        } 
        else {
            oldlast.next = last;
        }
        n++;
    }


    public Item dequeue() {
        if (isEmpty())
            throw new NoSuchElementException("Queue underflow");

        // How do you get the item from the front of the list,
        Item item = first.item;

        // and ensure that the next element is now the front?
        first = first.next;
        n--;
        // Watch out for empty queue
        if (isEmpty()) {
            last = null;
        }
     
        return item;

    }

    /**
     * Returns an iterator that iterates over the items in this queue in FIFO order.
     */
    public Iterator<Item> iterator() {
        return new ListIterator();
    }

    // an iterator, doesn't implement remove() since it's optional
    private class ListIterator implements Iterator<Item> {
        private Node current = first;

        public void remove() {
            throw new UnsupportedOperationException();
        }

        public boolean hasNext() {
            return ((current != null));
            // how to check if the iterator still has more things to work with?
        }

        public Item next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            Item item = current.item;
            current = current.next;
            return item;

            // how to walk along the list, starting from first ?
            // watch out for next() call when there is no next item
        }
    }

    /**
     * Unit tests the LinkedQueue data type.
     */
    public static void main(String[] args) {
        
        LinkedQueue<String> queue = new LinkedQueue<String>();
       
        
        while (!StdIn.isEmpty()) {
           
            String item = StdIn.readString();
            System.out.println(item);
            if (!item.equals("-"))
                queue.enqueue(item);
            else if (!queue.isEmpty())
                StdOut.print(queue.dequeue() + " ");
        }
        StdOut.println("(" + queue.size() + " left on queue)");
    }
}
