/******************************************************************************
 *  Compilation:  javac-algs4 TestAlgs4.java
 *  Execution:    java-algs4 TestAlgs4 
 *  Dependencies: StdDraw.java
 *                http://algs4.cs.princeton.edu/mac/cover.jpg
 *  
 *  Percolates a grid detecting connectivity with Weighted quick union. 
 * 
 ******************************************************************************/

import edu.princeton.cs.algs4.QuickFindUF;
import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {

    // initialize related variables
    private final boolean[][] grid;
    private final int source;
    private final int sink;
    private final int gridSize;
    private WeightedQuickUnionUF wqf = new WeightedQuickUnionUF(0);
    //private QuickFindUF wqf = new QuickFindUF(0);
    
    // Percolation constructor of size gridSize
    public Percolation(int gridSize) {

        if (gridSize <= 0) {
            throw new java.lang.IndexOutOfBoundsException();
        }
        source = gridSize * gridSize;
        this.gridSize = gridSize;
        sink = gridSize * gridSize + 1;
        grid = new boolean[gridSize][gridSize];
        wqf = new WeightedQuickUnionUF(gridSize * gridSize + 2);
        //wqf = new QuickFindUF(gridSize * gridSize + 2);



    }

    // open and check if nearby are open, if so union. 
    public void open(int row, int col) throws IndexOutOfBoundsException {

        grid[row][col]=true;

        if (row < (grid[0].length - 1) && isOpen(row + 1, col)) {
            wqf.union(row*gridSize+col, (row+1)*gridSize+col);
        }

        if (col < (grid[0].length - 1) && isOpen(row, col + 1)) {
            wqf.union(row*gridSize+col, row*gridSize+(col+1));
            
        }

        if (row > 0 && isOpen(row - 1, col)) {
            wqf.union(row*gridSize+col, (row-1)*gridSize+col);
       
        }

        if (col > 0 && isOpen(row, col - 1)) {
            wqf.union(row*gridSize+col, (row)*gridSize+(col-1));
            
        }

        if (row == 0) {

            wqf.union(row*gridSize+col, gridSize * gridSize);
        }
        if (row == gridSize - 1) {

            wqf.union(row*gridSize+col, gridSize * gridSize + 1);
        }
    }

    public boolean isOpen(int row, int col) throws IndexOutOfBoundsException {
        
        // Check if open.
        return grid[row][col];

    }

    public boolean isFull(int row, int col) throws IndexOutOfBoundsException {

        // Check if connected.
        return wqf.connected(row*gridSize+col, source);

    }

    // Count open sites.
    public int numberOfOpenSites() {
        int numOpen = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (grid[i][j]) {
                    numOpen++;
                }
            }
        }
        return numOpen;
    }

    // Check if system percolates
    public boolean percolates() {
        return wqf.connected(source, sink);
    }

    // Sites object for ease.
 
    
    // Unit testing in main 
    public static void main(String[] args) {
        Percolation temp = new Percolation(Integer.parseInt(args[0]));
    }


}
