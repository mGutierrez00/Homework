import java.io.IOException;
import java.util.Arrays;
import java.util.stream.IntStream;

import edu.princeton.cs.algs4.BreadthFirstDirectedPaths;
import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.In;

public class ShortestCommonAncestor {
    BreadthFirstDirectedPaths bfspaths;
    Digraph G;
    String path;

    // constructor takes a rooted DAG as argument
    public ShortestCommonAncestor(Digraph G) {
        this.G = G;

    }

    // length of shortest ancestral path between v and w
    public int length(int v, int w) {
        if(v == w) return 0;
        path = "";
        String pathV = recursiveRoot(v);
        path = "";
        String pathW = recursiveRoot(w);
        //System.out.println(pathV);
        //System.out.println(pathW);
        String[] pathAV = pathV.split(",");
        String[] pathAW = pathW.split(",");

        int i = 0;
        while (i < pathAV.length && i < pathAW.length && pathAV[i].equals(pathAW[i])) {
            i++;
        }
     
        return pathAV.length + 1 - i + pathAW.length + 1 - i;
    }

    // a shortest common ancestor of vertices v and w
    public int ancestor(int v, int w) {
        if(v == w) return v;
        path = "";
        String pathV = recursiveRoot(v);
        path = "";
        String pathW = recursiveRoot(w);
        String[] pathAV = pathV.split(",");
        String[] pathAW = pathW.split(",");
        for(String n : pathAV) {
            System.out.println(n);
        }
        int i = 0;
       
        while (i < pathAV.length && i < pathAW.length && pathAV[i].equals(pathAW[i])) {
            i++;
        }
    
        return Integer.parseInt(pathAV[i - 1]);

    }

    private String recursiveRoot(int v) {
       
        if (G.outdegree(v) == 0) {
            return path;
        }
        for (int n : G.adj(v)) {

            recursiveRoot(n);
            path = path + n + ",";
            
        }
        
               
        return path;
    }

    // length of shortest ancestral path of vertex subsets A and B
    public int length(Iterable<Integer> subsetA, Iterable<Integer> subsetB) {
        int min = 100000;
        for (int i : subsetA) {
            for (int j : subsetB) {
                int temp = length(i, j);

                if (length(i, j) < min) {

                    min = length(i, j);

                }
            }
        }
        return min;
    }

    // a shortest common ancestor of vertex subsets A and B
    public int ancestor(Iterable<Integer> subsetA, Iterable<Integer> subsetB) {
        int min = 100000;
        int ancestor = 0;
        for (int i : subsetA) {
            for (int j : subsetB) {
                int temp = length(i, j);

                if (temp < min) {

                    min = temp;
                    ancestor = ancestor(i, j);
                }
            }
        }
        return ancestor;
    }

    // do unit testing of this class
    public static void main(String[] args) throws IOException {
        In in = new In("digraph1.txt");
        Digraph G = new Digraph(in);
        ShortestCommonAncestor sca = new ShortestCommonAncestor(G);
        WordNet wordnet = new WordNet("Synsets.txt", "Hypernyms.txt");
       // wordnet.distance(noun1, noun2)
        
        // int v = 3;
        // int w = 10;
        Integer v[] = {4};
        Integer w[] = {7};
        Iterable<Integer> iterablev = Arrays.asList(v);
        Iterable<Integer> iterablew = Arrays.asList(w);
        int length = sca.length(iterablev, iterablew);
        int ancestor = sca.ancestor(iterablev, iterablew);
        StdOut.printf("length = %d, ancestor = %d\n", length, ancestor);

        /*
         * In in = new In(args[0]); Digraph G = new Digraph(in); ShortestCommonAncestor
         * sca = new ShortestCommonAncestor(G); while (!StdIn.isEmpty()) { int v =
         * StdIn.readInt(); int w = StdIn.readInt(); int length = sca.length(v, w); int
         * ancestor = sca.ancestor(v, w); StdOut.printf("length = %d, ancestor = %d\n",
         * length, ancestor); }
         */

    }
}