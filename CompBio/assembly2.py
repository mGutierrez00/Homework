from __future__ import print_function
import sys
import time

class Node(object):
    def __init__(self, previous, depth, base, idx):

        self.next = {"A":None,"T":None,"G":None,"C":None}
        self.depth = depth
        self.base = base
        self.idx = idx
        self.previous = previous


class seqTree(object):
    def __init__(self,reads,threshold):
        self.threshold = threshold
        self.reads = reads
        self.head = None
        self.matchList = {}
        self.leaves = []
        self.matched = set()
        self.toRemove = []
        if self.head is None:
            self.head = Node(None,0,"$",None)

        self.add(reads,threshold)

        start_time = time.time()
        self.iterTraverse(self.leaves)
        print("Traverse Time = %s" % (time.time() - start_time))

    def add(self,reads,threshold):
        for idx,read in enumerate(reads):
            suffixes = [read[i:] for i in range(0,len(read))]
            readUseless = True
            for suffix in suffixes:
                current = self.head
                sufUseless = True
                if(len(suffix)>30):
                    for i in range(0,30):
                        if current.next.get(suffix[i]) is None:
                            sufUseless = False
                            readUseless = False
                            new = Node(current, current.depth+1,suffix[i],idx)
                            current.next.update({suffix[i]:new})
                            current = new
                        else:
                            current = current.next.get(suffix[i])
                else:
                    for base in suffix:
                        if current.next.get(base) is None:
                            sufUseless = False
                            readUseless = False
                            new = Node(current, current.depth+1,base,idx)
                            current.next.update({base:new})
                            current = new
                        else:
                            current = current.next.get(base)

                if not sufUseless and current.depth > threshold:
                    self.leaves.append(current)
            if readUseless:
                self.toRemove.append(idx)


    def iterTraverse(self,leaves):
        for leaf in leaves:
            head = leaf
            while leaf.previous is not None:
                    leaf = leaf.previous
                    if leaf.idx != head.idx  and leaf.idx not in self.matched and head.idx not in self.matched and leaf.depth > self.threshold:
                        list = [str(leaf.idx),str(head.idx)]
                        list = ','.join(list)
                        self.matchList[list]=leaf.depth
                        self.matched.add(leaf.idx)
                        self.matched.add(head.idx)
                        break

def combine(str1, str2):
    def find(str1,str2):
        bestAlign = 0
        bestScore = 0
        for idx,char in enumerate(str1):
            i = 0
            score = 0
            if (char == str2[i]):
                j = i
                while(idx+i < len(str1) and j < len(str2) and str1[idx+i] == str2[j]):
                    score+=1
                    i+=1
                    j+=1
                if (idx+i == len(str1) or j == len(str2)) and (bestScore < score):
                    bestScore = score
                    bestAlign = idx
                i+=1
                score = 0
        return[bestAlign,bestScore]
    def align(str1, str2,oneAndTwo, TwoAndOne):

        if(oneAndTwo[1]>TwoAndOne[1]):
            return str1+(str2[oneAndTwo[1]:])
        else:
            return str2+(str1[TwoAndOne[1]:])

    oneAndTwo = find(str1,str2)
    TwoAndOne = find(str2,str1)

    return(align(str1,str2,oneAndTwo, TwoAndOne))

def main(argv):
    reads = []
    chosenFile = argv[0]
    Total_time = time.time()
    with open(chosenFile) as file:
        for line in file:
                line = line.strip()
                reads.append(line)


    for threshold in range(30,9,-1):
        start_time = time.time()
        assembly = seqTree(reads,threshold)
        print("Tree time = %s" % (time.time() - start_time))
        deleted = []

        for match in assembly.matchList:
            match = match.split(',')
            read1 = reads[int(match[0])]
            read2 = reads[int(match[1])]
            start_time = time.time()
            aligned = combine(read1,read2)
            reads[int(match[0])] = aligned
            deleted.append(int(match[1]))
        deleted = deleted + assembly.toRemove
        deleted = sorted(deleted, reverse = True)
        for idx in deleted:
            del reads[idx]
        print("threshold", threshold)
        print("Read Length", len(reads))
        print("Number of Leaves:", len(assembly.leaves))
        maxRead = max(reads, key=len)

        del assembly
    print(maxRead)
    f = open("output", "w")
    f.write(maxRead)
    print("---Total Time %s seconds ---" % (time.time() - Total_time))

if __name__=='__main__':
  main(sys.argv[1:])
