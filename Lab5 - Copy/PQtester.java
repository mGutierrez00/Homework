import java.util.Random;

import edu.princeton.cs.algs4.MaxPQ;

public class PQtester {

	public static void main(String[] args) {
		int average=0; 
		Random rand = new Random();
		
		for (int j = 0; j < 10; j++) {
			
			long startTime = System.nanoTime();

			//int n = Integer.parseInt(args[0]);
			int n = 10000000;
			
			MaxPQ mpq = new MaxPQ(n);

			for (int i = 0; i < n; i++) {
				mpq.insert(rand.nextInt(n));
			}
			for (int i = 0; i < mpq.size() / 2; i++) {
				mpq.max();
			}
			for (int i = 0; mpq.size() < n; i++) {
				mpq.insert(i);
			}
			while (mpq.isEmpty()) {
				mpq.max();
			}
			long estimatedTime = System.nanoTime() - startTime;

			average += estimatedTime;
		}
		average = average/10;
		
		System.out.println(average);
	}

}
