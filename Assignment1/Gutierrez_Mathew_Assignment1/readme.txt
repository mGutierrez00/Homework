/******************************************************************************
 *  Name:     Mathew Gutierrez
 *  NetID:    790 657 974
 *  Precept:  
 *
 *  Partner Name:    N/A
 *  Partner NetID:   N/A
 *  Partner Precept: N/A
 * 
 *  Operating system: Ubuntu
 *  Compiler:	Eclipse
 *  Text editor / IDE:	Eclipse
 *
 *  Have you taken (part of) this course before: No
 *  Have you taken (part of) the Coursera course Algorithm, Part I: No
 *
 *  Hours to complete assignment (optional): ~12
 *
 ******************************************************************************/

Programming Assignment 1: Percolation


/******************************************************************************
 *  Describe how you implemented Percolation.java. How did you check
 *  whether the system percolates?
 *****************************************************************************/

I implemented Percolation.java by first creating a grid of size N from the command line
Then I created a Weighted Quck union of size N*N + 2 to account for source and sink.  
Then for each cell in grid, I filled with a site object that has the attributes isOpen and ID
These objects can be opened from PercolationStats and controlled from there.

I checked to see if the system percolates with the percolates() method, where I 
collect the returned values from the WeightedQuickFind.connected(source,sink) to check 
if top and bottom are connected.

/******************************************************************************
 *  Perform computational experiments to estimate the running time of
 *  PercolationStats.java for values values of n and T when implementing
 *  Percolation.java with QuickFindUF.java.
 *
 *  To do so, fill in the two tables below. Each table must have at least
 *  4 data points, ranging in time from around 0.1 seconds to around
 *  60 seconds. Do not include data points that takes less than 0.1 seconds.
 *****************************************************************************/

(keep T constant)

 n          time (seconds) T = 10
------------------------------

				Ratio
100		.154		
200		2.335		15.16
400		37.223		15.9
600		146.302		

Ratio approaches limit of 2^4

therefor Order of growth is N^4


(keep n constant)

 T          time (seconds) N = 100
------------------------------
				Ratio
200		2.837
400		5.641		1.988
800		11.254		1.995
1600		22.436		1.993
3200		44.806		1.997
6400		89.701		2.002

Ratio approaches a limit of 2^1

therefor Order of growth is ~N

/******************************************************************************
 *  Using the empirical data from the above two tables, give a formula 
 *  (using tilde notation) for the running time (in seconds) of
 *  PercolationStats.java as function of both n and T, such as
 *
 *       ~ 5.3*10^-8 * n^5.0 T^1.5
 *
 *  Recall that with tilde notation, you include both the coefficient
 *  and exponents of the leading term (but not lower-order terms).
 *  Round each coefficient to two significant digits.
 *
 *****************************************************************************/

running time (in seconds) as a function of n and T:  ~ 

7.09*10^-10*T*N^4 



/******************************************************************************
 *  Repeat the previous two questions, but using WeightedQuickUnionUF
 *  (instead of QuickFindUF).
 *****************************************************************************/

(keep T constant)

 n         time (seconds) T = 10
------------------------------
				Ratios
100		.019		2.57
200		.049		2.14
400		.105		3.95
800		.415		7.34
1600		3.04		5.85
3200		17.79		4.54
6400		80.79				

(keep n constant)

Ratio approaches a limit of ??? ~~2^2

therefor Order of growth is ~N^2(?)

 T          time (seconds) N = 100
------------------------------
				Ratios
200		.139		1.87
400		.26		1.93
800		.501		1.93
1600		.967
3200		1.88
6400		3.708	
12800		7.36
25600		14.67
51200		29.656
102400		58.66

Ratio approaches a limit of 2^1

therefor Order of growth is ~N


running time (in seconds) as a function of n and T:  ~ 

1.84*10^-11*T*N^2

/**********************************************************************
 *  How much memory (in bytes) does a Percolation object (which uses
 *  WeightedQuickUnionUF.java) use to store an n-by-n grid? Use the
 *  64-bit memory cost model from Section 1.4 of the textbook and use
 *  tilde notation to simplify your answer. Briefly justify your
 *  answers.
 *
 *  Include the memory for all referenced objects (deep memory).
 **********************************************************************/

For the percolation grid of n-by-n, there are n*n boolean, each of which takes 1 byte.	=N^2 Bytes

 
/******************************************************************************
 *  Known bugs / limitations.
 *****************************************************************************/

Wasn't able to implement bug finder. Not sure if throws are protected as required


/******************************************************************************
 *  Describe whatever help (if any) that you received.
 *  Don't include readings, lectures, and precepts, but do
 *  include any help from people (including course staff, lab TAs,
 *  classmates, and friends) and attribute them by name.
 *****************************************************************************/

Asked Travis about more elegant solution to Colate 2d array to 1d array using unique identifiers.
He suggested calculating using id=row(gridsize)+col  which using my 20-20 hindsight is an easy and obvious answer

/******************************************************************************
 *  Describe any serious problems you encountered.                    
 *****************************************************************************/

I had issues with calculating the empirical formula to get time. I think its mostly
due to my math skills being rusty.


/******************************************************************************
 *  List any other comments here. Feel free to provide any feedback   
 *  on how much you learned from doing the assignment, and whether    
 *  you enjoyed doing it.                                             
 *****************************************************************************/
I enjoyed the assignment quite a lot. I would have liked a little more guidance on 
calculating the empirical run time however.

