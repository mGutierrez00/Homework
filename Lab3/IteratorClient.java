import java.util.Iterator;

public class IteratorClient {

    public static void main(String[] args) {
        RandomizedBag<String> bag = new RandomizedBag<String>();

        bag.put("we");
        bag.put("are");
        bag.put("the");
        bag.put("music-makers");
        bag.put("and");
        bag.put("dreamers");
        bag.put("of");
        bag.put("dreams");

        Iterator<String> itr1 = bag.iterator();
        if (!bag.isEmpty())
            bag.get(); // test removal of one
        Iterator<String> itr2 = bag.iterator();

        /*
         * In order to make it so removing from itr2 does not affect itr1, I need to not
         * remove the item from the bag, but rather remove the index from the shuffled
         * list of indexes. But that would leave me no recourse to actually removing an
         * element from the bag.
         */

        StdOut.println("Here's what was left before removing one (in random order):");
        while (itr1.hasNext()) {
            String s = itr1.next();
            StdOut.print(s + " ");
        }
        StdOut.println("");

        StdOut.println("Here's what was left after removing one (in random order):");
        while (itr2.hasNext()) {
            String s = itr2.next();
            StdOut.print(s + " ");
        }
        StdOut.println("");

    }
}