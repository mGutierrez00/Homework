import sys
import numpy


def main(argv):
    size = argv[0]
    seq = [];
    for x in range (int(size)):
        base = numpy.random.randint(0,3)
        seq.append(base)
    print(numToAlpha(seq))


def numToAlpha(seq):
    for i,x in enumerate(seq):
        if x == 0: seq[i] = 'A'
        if x == 1: seq[i] = 'T'
        if x == 2: seq[i] = 'G'
        if x == 3: seq[i] = 'C'
    return "".join(seq);

if __name__=='__main__':
  main(sys.argv[1:])
