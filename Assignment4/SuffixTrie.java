import java.awt.RenderingHints.Key;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.TrieST;

public class SuffixTrie {

    public SuffixTrie(String S, String Q) {
        TrieST<Integer> st = new TrieST<Integer>();
        In inS = new In(S);
        In inQ = new In(Q);
        while (inS.hasNextLine()) {
            String sequence = inS.readLine();
            String subSequence = sequence;
            for (int i = 0; i+1 < sequence.length(); i++) {
                st.put(subSequence, i);
                subSequence = subSequence.substring(1, subSequence.length());
                StdOut.println(i + " : " + subSequence);
            }
        }
        while (inQ.hasNextLine()) {
            String query = inQ.readLine();
            StdOut.println(query);
            for (String s : st.keysWithPrefix(query)) {
                StdOut.println(st.get(s));
            }
            StdOut.println();
        }
    }

    public static void main(String[] args) {
        In in = new In(args[0]);
        SuffixTrie trie = new SuffixTrie(args[0], args[1]);

    }

}
