/******************************************************************************
 *  Compilation:  javac RandomizedBag.java
 *  Execution:    java RandomizedBag < input.txt
 *  Dependencies: StdIn.java StdOut.java
 *  
 *  Randomized bag with get put independent random iterators
 ******************************************************************************/

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Random;

public class RandomizedBag<Item> implements Iterable<Item> {

    private Item[] a;
    private int n;
    private Random rand = new Random();

    public RandomizedBag() {

        a = (Item[]) new Object[1];
        n = 0;
    }

    // Check is empty
    public boolean isEmpty() {
        return n == 0;
    }

    // Return size
    public int size() {
        return n;
    }

    // Put in bag
    public void put(Item item) throws NullPointerException {
        if (item == null) {
            throw new java.lang.NullPointerException("New item cannot be NULL");
        }
        if (n == a.length) {
            resize(2 * a.length);
        }

        a[n] = item;
        n++;
    }

    // get from bag (remove)
    public Item get() throws NoSuchElementException {

        if (isEmpty()) {
            throw new java.util.NoSuchElementException("Cannot grab from empty bag.");
        }

        int i = rand.nextInt(n);

        Item tempItem = a[i];

        a[i] = a[n - 1];
        a[n - 1] = null;

        if (n <= a.length / 4) {
            resize(a.length / 2);
        }
        n--;
        return tempItem;
    }

    // Look in bag (dont remove)
    public Item sample() throws NoSuchElementException {

        if (isEmpty()) {
            throw new java.util.NoSuchElementException("Cannot grab from empty bag.");
        }

        int i = rand.nextInt(n);
        return a[i];
    }

    // unit testing
    // Move to new bigger/smaller bag.
    private void resize(int capacity) {
        Item[] temp = (Item[]) new Object[capacity];

        for (int i = 0; i < n; i++) {
            temp[i] = a[i];
        }

        a = temp;
    }

    // Return new iterator
    public Iterator<Item> iterator() { // return an independent iterator over items in random order
        return new RandBagIterator();

    }

    // Iterator class
    private class RandBagIterator implements Iterator<Item> {

        private int i = 0;
        private int[] indices;

        public RandBagIterator() {
            indices = new int[n];
            for (int j = 0; j < indices.length; j++) {
                indices[j] = j;
            }

            StdRandom.shuffle(indices);
        }

        public boolean hasNext() {
            return i < n;
        }

        public Item next() throws java.util.NoSuchElementException {
            if (!hasNext()) {
                throw new java.util.NoSuchElementException("No more items in iteration.");
            }
            return a[indices[i++]];
        }

        public void remove() throws UnsupportedOperationException {
            throw new java.lang.UnsupportedOperationException("Remove in iterator is not supported");
        }
    }

    // Unit testing
    public static void main(String[] args) {

        RandomizedBag<String> bag = new RandomizedBag<String>();

        while (!StdIn.isEmpty()) {

            String item = StdIn.readString();
            bag.put(item);
        }

        while (!bag.isEmpty()) {
            System.out.println(bag.get());
        }

    }

}
